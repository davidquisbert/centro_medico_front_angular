import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { CollaboratorService } from '../services/collaborator.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private collaboratorService: CollaboratorService
  ) {}

  canActivate(): boolean {
    const user = this.collaboratorService.getUserLoggedIn();
      if ( user === null || user === undefined ) {
        window.location.href = '/';
        return false;
      }
    return true;
  }

 
}
