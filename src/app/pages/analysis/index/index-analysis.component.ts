import {Component, OnInit} from '@angular/core';
import {AnalysisOrderServiceService} from "../../../services/analysis-order.service.service";
import {RespuestaApi} from "../../../interfaces/interfaces";

@Component({
  selector: 'app-index-analysis',
  templateUrl: './index-analysis.component.html',
  styleUrls: ['./index-analysis.component.css']
})
export class IndexAnalysisComponent implements OnInit{

  analysis: any;
  observacion = '';
  idTipoEntrega = 0;
  ordenLaboratorio = 0;
  constructor(
    private analysisService: AnalysisOrderServiceService
  ) {
  }

  ngOnInit() {
    this.getAllAnalysis();
  }

  getAllAnalysis() {
    this.analysisService.getAllAnalysis()
      .subscribe(
        (response: RespuestaApi<any>) => {
          if (response.codigo === 0) {
            this.analysis = response.data;
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  postRegisterOrder(): void {
    this.analysisService.registerOrder(this.observacion, this.idTipoEntrega)
      .subscribe(
        (response: RespuestaApi<any>) => {
          if (response.codigo === 0) {
            console.log(response.mensaje);
            this.ordenLaboratorio = response.data.id;
            this.obtenerAnalysis();
          } else {
            console.log(response.mensaje);
          }
        },
        err => {
          console.log(err);
        }
      );
  }

  obtenerAnalysis(): void {
      const idsSeleccionados: number[] = [];
    this.analysis.forEach((dato: any) => {
      if (dato.seleccionado) {
        idsSeleccionados.push(dato.id);
      }
      });
      console.log(idsSeleccionados);

    for (let i = 0; i < idsSeleccionados.length; i++) {
      const id = idsSeleccionados[i];
      this.analysisService.registerAnalysis(this.ordenLaboratorio, id)
        .subscribe(
          (response: RespuestaApi<any>) => {
            if (response.codigo === 0) {
              console.log(response.mensaje);
            } else {
              console.log(response.mensaje);
            }
          },
          err => {
            console.log(err);
          }
        );
    }
  }


}
